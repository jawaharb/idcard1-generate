package model;

public class Detail {
	private String rollno;
	private String name;
	private String dob;
	private String department;
	private String yearofjoining;
	private String yearofpassedout;
	private String bloodgroup;
	private String address;
	private Long contact;
		public Detail() {
			// TODO Auto-generated constructor stub
		}
		public Detail(String rollno, String name, String dob, String department, String yearofjoining,
				String yearofpassedout, String bloodgroup, String address, Long contact) {
			super();
			this.rollno = rollno;
			this.name = name;
			this.dob = dob;
			this.department = department;
			this.yearofjoining = yearofjoining;
			this.yearofpassedout = yearofpassedout;
			this.bloodgroup = bloodgroup;
			this.address = address;
			this.contact = contact;
		}
		public String getRollno() {
			return rollno;
		}
		public void setRollno(String rollno) {
			this.rollno = rollno;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDob() {
			return dob;
		}
		public void setDob(String dob) {
			this.dob = dob;
		}
		public String getDepartment() {
			return department;
		}
		public void setDepartment(String department) {
			this.department = department;
		}
		public String getYearofjoining() {
			return yearofjoining;
		}
		public void setYearofjoining(String yearofjoining) {
			this.yearofjoining = yearofjoining;
		}
		public String getYearofpassedout() {
			return yearofpassedout;
		}
		public void setYearofpassedout(String yearofpassedout) {
			this.yearofpassedout = yearofpassedout;
		}
		public String getBloodgroup() {
			return bloodgroup;
		}
		public void setBloodgroup(String bloodgroup) {
			this.bloodgroup = bloodgroup;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public Long getContact() {
			return contact;
		}
		public void setContact(Long contact) {
			this.contact = contact;
		}
		
}
