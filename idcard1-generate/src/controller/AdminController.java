package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDAO;
import dao.AdminDAOImp;
import model.Admin;
import model.Detail;

@WebServlet(name = "AdminController", urlPatterns = { "/add", "/login", "/updateCon" })
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url = request.getServletPath();
		AdminDAO dao = new AdminDAOImp();
		if (url.equals("/login")) {
			String uid = request.getParameter("uid");
			String password = request.getParameter("password");
			HttpSession session = request.getSession(true);
			session.setAttribute("uid", uid);
			Admin admin = new Admin(uid, password);
			
			int result = dao.adminAuthentication(admin);
			if (result > 0) {
				List<Detail> detail = dao.viewAllDetail();
				request.setAttribute("data", detail);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("error", "Hi " + uid + " please check username and password");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		} else if (url.equals("/add")) {
			String rollNo = request.getParameter("RollNo");
			String name = request.getParameter("Name");
			String dob = request.getParameter("Dob");
			String department = request.getParameter("Department");
			String yearOfJoining = request.getParameter("YearOfJoining");
			String yearOfPassedout = request.getParameter("YearOfPassedout");
			String bloodGroup = request.getParameter("BloodGroup");
			String address = request.getParameter("Address");
			Long contact = Long.parseLong(request.getParameter("ContactNo"));
			Detail detail = new Detail(rollNo, name, dob, department, yearOfJoining, yearOfPassedout, bloodGroup,
					address, contact);
			
			int result = dao.addData(detail);
			if (result > 0) {
				request.setAttribute("msg", rollNo + " inserted successfully");
			} else {
				request.setAttribute("msg", rollNo + " not inserted successfully");
			}
			List<Detail> detail1 = dao.viewAllDetail();
			request.setAttribute("data", detail1);
			request.getRequestDispatcher("adminopt.jsp").forward(request, response);

		} else if (url.equals("/updateCon")) {
			String rollNo = request.getParameter("RollNo");
			String name = request.getParameter("Name");
			String address = request.getParameter("Address");
			Long contact = Long.parseLong(request.getParameter("ContactNo"));

			Detail detail = new Detail();
			detail.setRollno(rollNo);
			detail.setName(name);
			detail.setAddress(address);
			detail.setContact(contact);
	
			dao.updateInfo(detail);
			List<Detail> detail1 = dao.viewAllDetail();
			request.setAttribute("data", detail1);
			request.getRequestDispatcher("adminopt.jsp").include(request, response);

		}

	}
}
