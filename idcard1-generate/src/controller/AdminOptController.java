package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AdminDAO;
import dao.AdminDAOImp;
import model.Detail;

@WebServlet(name = "AdminOptController", urlPatterns = { "/adminopt", "/home", "/delete", "/insert", 
		"/signout","/update" })
public class AdminOptController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String url=request.getServletPath(); 
		AdminDAO dao = new AdminDAOImp();
		Detail detail = new Detail();
		
		if(url.equals("/adminopt")) {
			List<Detail> detail1 = dao.viewAllDetail();
			request.setAttribute("data", detail1);
			request.getRequestDispatcher("adminopt.jsp").include(request, response);
		
		}else if(url.equals("/home")) {
			List<Detail> detail1 = dao.viewAllDetail();
			request.setAttribute("data", detail1);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
			
		}else if(url.equals("/delete")) {
			String rollNo = request.getParameter("RollNo");
			
			detail.setRollno(rollNo);
			dao.removeData(detail);
			PrintWriter out = response.getWriter();
			response.setContentType("text/html");
			out.print(rollNo+" deleted sucessfully<br/>");
			List<Detail> detail1 = dao.viewAllDetail();
			request.setAttribute("data", detail1);
			request.getRequestDispatcher("adminopt.jsp").include(request, response);
			
		}else if(url.equals("/insert")) {
			request.getRequestDispatcher("insert.jsp").forward(request, response);
			
		}else if(url.equals("/signout")) {
			HttpSession session = request.getSession(false);
			String uid =(String) session.getAttribute("uid");
			request.setAttribute("uid", uid+" logout sucessfully");
			request.getRequestDispatcher("index.jsp").forward(request, response);
			
		}else if(url.equals("/update")) {
			String rollNo = request.getParameter("RollNo");
			String name = request.getParameter("Name");
			String address= request.getParameter("Address");
			Long contact=  Long.parseLong(request.getParameter("ContactNo"));
			detail.setRollno(rollNo);
			detail.setName(name);
			detail.setAddress(address);
			detail.setContact(contact);
			request.setAttribute("data",detail);
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}
	}

}
