package dao;

import java.util.List;

import model.Admin;
import model.Detail;

public interface AdminDAO {
	public int adminAuthentication(Admin admin);
	public List<Detail> viewAllDetail();
	public void removeData(Detail detail);
	public void updateInfo(Detail detail);
	public int addData(Detail detail);
}
