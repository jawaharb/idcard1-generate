package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Detail;
import util.DbUtil;
import util.QueryUtil;

public class AdminDAOImp implements AdminDAO{
	PreparedStatement pst = null;
	ResultSet rs = null;
	int result;
	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.adminAuth);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			rs = pst.executeQuery();
			while(rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception in Admin");
		}finally {
			
			try {
				pst.close();
				rs.close();
				DbUtil.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
		return result;
	}
	@Override
	public List<Detail> viewAllDetail() {
		List<Detail> list = new ArrayList<Detail>();
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.viewAll);
			rs = pst.executeQuery();
			while(rs.next()) {
				Detail detail = new Detail(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6),
						rs.getString(7),rs.getString(8),rs.getLong("contact"));
				list.add(detail);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception in ViewAll");
		}finally {
			try {
				DbUtil.getConnection().close();
				pst.close();
				rs.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return list;
	}
	@Override
	public void removeData(Detail detail) {
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.removeData);
			pst.setString(1, detail.getRollno());
			result = pst.executeUpdate();
		}catch (ClassNotFoundException | SQLException e) {
			System.out.println(e);
			e.printStackTrace();
			System.out.println("Exception Error in Deletion");
		}finally {

				try {
					DbUtil.getConnection().close();
					pst.close();
				} catch (ClassNotFoundException | SQLException e) {
					
				}
		}
	}
	@Override
	public void updateInfo(Detail detail) {
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.updateInfo);
			pst.setString(1, detail.getName());
			pst.setString(2, detail.getAddress());
			pst.setLong(3, detail.getContact());
			pst.setString(4, detail.getRollno());
			result = pst.executeUpdate();
		}catch (ClassNotFoundException | SQLException e) {
			System.out.println(e);
			e.printStackTrace();
			System.out.println("Exception Error in update Info");
		}finally {

				try {
					DbUtil.getConnection().close();
					pst.close();
				} catch (ClassNotFoundException | SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
	}
	@Override
	public int addData(Detail detail) {
		result =0;
		try {
			pst = DbUtil.getConnection().prepareStatement(QueryUtil.addData);
			pst.setString(1,detail.getRollno());
			pst.setString(2, detail.getName());
			pst.setString(3, detail.getDob());
			pst.setString(4, detail.getDepartment());
			pst.setString(5, detail.getYearofjoining());
			pst.setString(6, detail.getYearofpassedout());
			pst.setString(7, detail.getBloodgroup());
			pst.setString(8, detail.getAddress());
			pst.setLong(9, detail.getContact());
			result = pst.executeUpdate();
			
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			System.out.println("Exception occur in add data");
			return result;
		}finally {
			try {
				DbUtil.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}
}
