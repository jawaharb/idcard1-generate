<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Operation</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<h1>Admin Operation Page</h1>
	<hr />
	<p align ="center"> Welcome <%=(String)session.getAttribute("uid")%></p>
	<p align="right">
	<a href="home">Home</a>
	<a href="insert">Add New Data</a>
	<a href="signout">logout</a>
	</p>
	<hr />
	${msg}
	<table>
	<tr><th colspan="11">Student Detail</th></tr>
	<tr><th>Roll No</th><th>Name</th><th>DOB</th><th>Deparment</th><th>Joining Date</th><th>PassedOut Date </th>
	<th>Blood Group</th><th>Address</th><th>Contact</th><th>Update</th><th>Delete</th></tr>
	
	
	<c:forEach items ="${data}" var ="d">
	<tr style ="text-align: center;"><td>${d.getRollno()}</td><td>${d.getName()}</td><td>${d.getDob()}</td>
	<td>${d.getDepartment()}</td><td>${d.getYearofjoining()}</td><td>${d.getYearofpassedout()}</td>
	<td>${d.getBloodgroup()}</td><td>${d.getAddress()}</td><td>${d.getContact()}</td>
	<td><a href ="update?RollNo=${d.getRollno()}&Name=${d.getName()}&Address= ${d.getAddress()}
	&ContactNo=${d.getContact()}">Update</a></td>
	<td><a href ="delete?RollNo=${d.getRollno()}">Delete</a></td>
	</tr>
	
	</c:forEach> 
	</table>
</body>
</html>