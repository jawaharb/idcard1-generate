<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>update</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<h1>Admin Update Student Data Page</h1>
	<hr>
	<p align ="center"> Welcome <%=(String)session.getAttribute("uid")%></p>
	<p align="right">
		<a href ="adminopt">Back </a>
	  <hr>
	<form action="updateCon" method="post">
		RollNo:<input type="text" name="RollNo" readonly="readonly" value="${data.getRollno()}"/> <br />
	 <br /> Name:<input type="text" name="Name" value="${data.getName()}" /><br />
		<br /> Address:<input type="text" name="Address"
			value="${data.getAddress()}" /><br />
		<br /> Contact:<input type="text" name="ContactNo"
			value="${data.getContact()}" /><br />
		<br /> <input type="submit" value="update"/> 
	</form>
</body>
</html>